<?php
// Custom TinyMce styles

// Callback function to insert 'styleselect' into the $buttons array
function db_mce_buttons_2( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', __NAMESPACE__.'\\db_mce_buttons_2' );

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
  // Define the style_formats array
  $style_formats = array(
    // Each array child is a format with it's own settings
    array(
      'title' => 'Introduction text',
      'selector' => 'p',
      'classes' => 'introduction'
    ),
    array(
      'title' => 'Button Hollow Rose',
      'selector' => 'a',
      'classes' => 'button rose'
    ),
    array(
      'title' => 'Button Dark',
      'selector' => 'a',
      'classes' => 'button'
    ),
    array(
      'title' => 'Button Hollow Dark',
      'selector' => 'a',
      'classes' => 'button hollow'
    )
  );  
  // Insert the array, JSON ENCODED, into 'style_formats'
  $init_array['style_formats'] = json_encode( $style_formats );  
  
  return $init_array;  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', __NAMESPACE__.'\\my_mce_before_init_insert_formats' );  
