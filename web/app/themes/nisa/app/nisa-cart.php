<?php

namespace Nisa;

/**
 * Use custom checkout form
 */
add_action('template_redirect', __NAMESPACE__ . '\\nisa_cart_sent');
function nisa_cart_sent() {
  if( isset($_POST['action']) && $_POST['action'] == 'send-offer-cart' ) {
    $success = nisa_create_order($_POST);
    if( $success ) {
      $new_url = add_query_arg( 'kiitos', '', get_permalink() );
    } else {
      $new_url = add_query_arg( 'ei-kiitos', '', get_permalink() );
    }
    wp_redirect( $new_url, 303 );
  }
}

/**
 * Create order and empty cart
 */
function nisa_create_order($postdata) {
  $name_arr = explode(' ', sanitize_text_field($postdata['customer_name']));
  $last_name = array_pop($name_arr);
  $first_name = implode(' ', $name_arr);
  
  $address = array(
    'first_name' => $first_name,
    'last_name'  => $last_name,
    'company'    => sanitize_text_field($postdata['customer_company']),
    'email'      => sanitize_email($postdata['customer_email']),
    'phone'      => sanitize_text_field($postdata['customer_phone']),
    'address_1'  => sanitize_text_field($postdata['customer_address']),
    'city'       => sanitize_text_field($postdata['customer_city']),
    'postcode'   => sanitize_text_field($postdata['customer_postal_code']),
    'country'    => 'FI'
  );

  $args = array(
      'order_comments' => wc_sanitize_textarea($postdata['customer_message'])
    );

  $checkout = WC()->checkout();
  $order_id = $checkout->create_order($args);
  $order = wc_get_order( $order_id );
  $order->set_address( $address, 'billing' );
  $order->set_address( $address, 'shipping' );
  $order->calculate_totals();
  $order->payment_complete();
  WC()->cart->empty_cart();

  return true;
}
