<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller {
  public function siteName() {
    return get_bloginfo('name');
  }

  public static function title() {
    if (get_field('override_page_title')) {
      return get_field('override_page_title');
    }
    if (is_home()) {
      if ($home = get_option('page_for_posts', true)) {
        return get_the_title($home);
      }
      return __('Latest Posts', 'sage');
    }
    if (is_archive()) {
      return get_the_archive_title();
    }
    if (is_search()) {
      return sprintf(__('Search Results for %s', 'sage'), get_search_query());
    }
    if (is_404()) {
      return __('Not Found', 'sage');
    }
    return get_the_title();
  }

  public function cartIsEmpty() {
    return WC()->cart->is_empty();
  }

  public static function get_cart_link() {
    $count = WC()->cart->get_cart_contents_count();
    $html = '<a class="cart-contents" href="'.wc_get_cart_url().'" title="'. sprintf(__('%s tuotetta tarjouspyyntökorissa', 'nisa'), $count).'"><span class="icon-cart"></span>';
    if($count) {
      $html .= '<span class="cart-contents-count">('.$count.')</span>';
    }
    $html .= '</a>';
    return $html;
  }

  public static function nisa_steps() {
    ob_start();
    ?>
    <div class="nisa-steps">
      <div class="nisa-step">
        <div class="step-border"><div class="step-number">1</div></div>
        <div class="step-text"><?= __('Valitse tuotteet valikoimastamme ja laita ne koriin', 'nisa') ?></div>
      </div>
      <div class="nisa-step">
        <div class="step-border"><div class="step-number">2</div></div>
        <div class="step-text"><?= __('Lähetä tarjouspyyntö valituista tuotteista', 'nisa') ?></div>
      </div>
      <div class="nisa-step">
        <div class="step-border"><div class="step-number">3</div></div>
        <div class="step-text"><?= __('Lähetämme sinulle tarjouksemme mahd. pian!', 'nisa') ?></div>
      </div>
    </div>
    <?php
    return ob_get_clean();
  }

  public static function nisa_steps_contractual() {
    ob_start();
    ?>
    <div class="nisa-steps contractual">
      <div class="nisa-step">
        <div class="step-border"><div class="step-number">1</div></div>
        <div class="step-text"><?= __('Valitse tuotteet ja laita ne koriin', 'nisa') ?></div>
      </div>
      <div class="nisa-step">
        <div class="step-border"><div class="step-number">2</div></div>
        <div class="step-text"><?= __('Lähetä tilaus', 'nisa') ?></div>
      </div>
      <div class="nisa-step">
        <div class="step-border"><div class="step-number">3</div></div>
        <div class="step-text"><?= __('Toimitamme tuotteet toimipisteeseenne', 'nisa') ?></div>
      </div>
    </div>
    <?php
    return ob_get_clean();
  }

}
