<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Single extends Controller {

  public function postCategories() {
    $cat_output = '';
    $cats = get_the_category();
    foreach ($cats as $dog) {
      $cat_output .= $dog->name;
    }
    return $cat_output;
  }

  public function otherPosts() {
    $args = array(
        'numberposts' => 4,
        'exclude' => array(get_the_ID())
      );
    return get_posts( $args );
  }
}
