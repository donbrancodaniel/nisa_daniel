<?php

namespace App;

/**
 * Register options pages
 */
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'    => 'Site settings',
    'menu_title'    => 'Site settings',
    'menu_slug'     => 'site-settings',
    'capability'    => 'manage_options',
    'redirect'      => false
  ));
}

/**
 * Disable author pages
 */
add_action('template_redirect', __NAMESPACE__ . '\\disable_author_archives');
function disable_author_archives() {
  if (is_author()) {
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
  } else {
    redirect_canonical();
  }
}

/**
 * add custom image sizes
 */
add_action( 'after_setup_theme', __NAMESPACE__ . '\\nisa_image_sizes' );
function nisa_image_sizes() {
  add_image_size( 'nisa-sq-750', 750, 620, true ); // (cropped)
}

/**
 * Remove Soil's canonical tag to prevent duplicate canonical tags
 * $priority = 15 ensures that it's running after Soil.
 */
add_action('init', function () {
  remove_action('wp_head', 'Roots\\Soil\\CleanUp\\rel_canonical');
}, 15);

/**
 * Prevent loading of wpcf7 css and js on all pages
 */
//add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

/**
 * Fall back to wpcf7 js-datepicker for date field on Firefox and IE
 */
add_filter( 'wpcf7_support_html5_fallback', '__return_true' );

/**
 * Remove WPML language selector css
 */
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);

/**
 * Remove WPML generator crap
 */
global $sitepress;
remove_action( 'wp_head', array( $sitepress, 'meta_generator_tag' ) );

/**
 * Remove WPML metabox when editing posts
 */
add_action( 'admin_head', __NAMESPACE__ . '\\disable_icl_metabox', 99 );
function disable_icl_metabox() {
  global $post;
  if( $post && isset($post->post_type) ) {
    remove_meta_box( 'icl_div_config', $post->post_type, 'normal' );
  }
}

/*
* Yoast SEO Disable Automatic Redirects for
* Posts And Pages
* Credit: Yoast Development Team
* Last Tested: May 09 2017 using Yoast SEO Premium 4.7.1 on WordPress 4.7.4
*/
add_filter('wpseo_premium_post_redirect_slug_change', '__return_true' );
/*
* Yoast SEO Disable Automatic Redirects for
* Taxonomies (Category, Tags, Etc)
* Credit: Yoast Development Team
* Last Tested: May 09 2017 using Yoast SEO Premium 4.7.1 on WordPress 4.7.4
*/
add_filter('wpseo_premium_term_redirect_slug_change', '__return_true' );

/**
 * Remove hidden languages from sitemap
 */
add_filter('wpseo_sitemap_entry', function($url) {
  if (isset($url['loc']) && empty($url['loc'])) {
    return FALSE;
  } elseif (isset($url['loc']) && strpos($url['loc'], '?lang=') !== false) {
    return FALSE;
  }
  return $url;
});

/**
 * Change og:image size
 */
add_filter( 'wpseo_opengraph_image_size', __NAMESPACE__ . '\\ac_wpseo_image_size', 10, 1 );
function ac_wpseo_image_size( $string ) {
  return 'large';
}

/**
 * Remove relative links in sitemaps
 */
add_action( 'init', __NAMESPACE__ . '\\remove_relative_links_in_sitemaps' );
function remove_relative_links_in_sitemaps() {
  if ( ! isset( $_SERVER['REQUEST_URI'] ) ) {
    return;
  }

  // Use absolute urls for categories always to fix hreflang tag
  remove_filter( 'term_link', 'Roots\\Soil\\Utils\\root_relative_url', 10 );

  $request_uri = $_SERVER['REQUEST_URI'];
  $extension   = substr( $request_uri, -4 );

  if (false !== stripos($request_uri, 'sitemap') && in_array($extension, array('.xml', '.xsl'))) {
    remove_filter( 'bloginfo_url', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'the_permalink', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'wp_list_pages', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'wp_list_categories', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'the_tags', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'get_pagenum_link', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'get_comment_link', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'month_link', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'day_link', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'year_link', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'the_author_posts_link', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
    remove_filter( 'wp_get_attachment_url', 'Roots\\Soil\\Utils\\root_relative_url', 10 );
  }
}
