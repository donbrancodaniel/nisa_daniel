<?php
/**
 * Register custom post type "contractual_customer"
 * @return nothing
 */
add_action( 'init', __NAMESPACE__ . '\\nisa_register_cpt', 0 );
function nisa_register_cpt() {
  $labels = array(
    'name'               => _x( 'Sopimusasiakkaat', 'post type general name', 'nisa' ),
    'singular_name'      => _x( 'Sopimusasiakas', 'post type singular name', 'nisa' ),
  );

  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Customers that have their own order portal', 'nisa' ),
    'menu_icon'          => 'dashicons-businessman',
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => 20,
    'supports'           => array( 'title' )
  );

  register_post_type( 'contractual_customer', $args );
}
