export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    
    // Bind carousels
    $('.owl-carousel').owlCarousel({
      items:1,
      loop:true,
      margin:10,
      nav:true,
      dots:true,
    });

    // Bind search toggle
    $('#search-toggle').on('click', function(event) {
      event.preventDefault();
      if( $('#search-flyout').is(":visible") ) {
        $('#search-flyout').fadeOut(400);
        $('.arrow-up', $(this)).fadeOut(400);
      } else {
        $('#search-flyout').fadeIn(400);
        $('.arrow-up', $(this)).fadeIn(400);
      }

      // Bind blur click
      $(document).click(function(event) {
        if(!$(event.target).closest('#search-flyout, #search-toggle').length) {
          if($('#search-flyout').is(":visible")) {
            $('#search-flyout').fadeOut(400);
            $('.arrow-up', $(this)).fadeOut(400);
          }
        }
      });
    });

  },
};