import * as FilePond from 'filepond';

export default {
  finalize() {
    // Create a multi file upload component
    const inputElement = document.querySelector('#pond input');
    FilePond.create(inputElement, {
        allowMultiple: true,
        instantUpload: true,
        labelIdle: 'Pudota tiedostot tähän tai <span class="filepond--label-action">Selaa</span>',
        labelFileLoading: 'Ladataan',
        labelFileProcessing: 'Lähetetään',
        labelFileProcessingComplete: 'Lähetys valmis',
        labelFileProcessingAborted: 'Lähetys peruutettu',
        labelFileProcessingError: 'Virhe lähetyksessä',
        labelTapToCancel: 'keskeytä',
        labelTapToRetry: 'yritä uudelleen',
        labelTapToUndo: 'peruuta',
        server: {
          url: nisa_vars.upload_api_url, // eslint-disable-line
          revert: {
            method: 'POST',
            headers: {
              'X-REST-Method': 'DELETE',
            },
          },
        },
    });
  },
};
