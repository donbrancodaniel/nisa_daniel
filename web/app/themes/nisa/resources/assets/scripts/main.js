// import external dependencies
import 'jquery';
import 'owl.carousel';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import templateCart from './routes/cart';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Cart
  templateCart,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
