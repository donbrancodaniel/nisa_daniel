<header class="site-header">
  <div class="header-wrapper">

    <a class="logo" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }} logo</a>

    <nav class="nav-primary">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </nav>

    <div class="secondary">
      <a class="button rose" href="tel:098789970">{{ __('Soita', 'nisa') }} 09 878 9970</a>
      <a id="search-toggle" href="#"><span class="icon-search"></span><div class="arrow-up"></div></a>
      {!! App::get_cart_link() !!}
    </div>

    <div id="search-flyout">
      {!! get_search_form(false) !!}
      <div class="text">
        <h2>{{ __('Vai etsitkö jotain tiettyä tuotetta? Tutustu tuoteosioon', 'nisa') }}</h2>
      </div>
      <a class="button hollow" href="#">Siirry tuotteisiin</a>
    </div>

  </div>
</header>
