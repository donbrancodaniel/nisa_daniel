<article @php post_class() @endphp>
  <header class="container-fluid">
    <h6>{!! $post_categories !!}</h6>
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
    {!! wp_get_attachment_image(get_post_thumbnail_id(), 'medium') !!}
  </header>
  <div class="entry-content container">
    <div class="row">
      <div class="col-12 col-md-9 offset-md-3">
        @php the_content() @endphp
      </div>
    </div>
  </div>

  @if($other_posts)
  <footer class="container">
    <div class="row other-articles">
      <div class="col-12">
        <h3>{{ __('Muita artikkeleita', 'nisa') }}</h3>
      </div>
      @foreach($other_posts as $post)
      <div class="col-md-3">
        <a href="{{ get_permalink($post->ID) }}">
          {!! wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'medium') !!}
          <h4>{!! get_the_title($post->ID) !!}</h4>
        </a>
      </div>
      @endforeach
    </div>
  </footer>
  @endif
</article>
