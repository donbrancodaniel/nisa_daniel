@php
  $text = get_sub_field('text');
  $aligned = get_sub_field('alignment');  
  $text_position = get_sub_field('text_position');  
  $img_position = get_sub_field('image_position');
  $margin_position = get_sub_field('margin');

  $size = wp_is_mobile() ? 'medium' : 'large';
  $img = get_sub_field('image');
  $order = $img_position == 'left' ? 'left' : 'reorder';
  $position_text = $text_position == 'top' ? 'top' : 'bottom'
@endphp

<div class='container'>
  <div class='row {{ $margin_position }}'>
    <div class='col-md-12 image_and_text_square {{ $order }}'>
      <div class='col-md-7 image_and_text_square__image' style="background-image: url('<?= $img['sizes'][$size]; ?>')"></div>
      <div class='col-md-5 image_and_text_square__content {{ $position_text }}'>
        <div class='image_and_text_square__content-text'>
          {!! $text !!}
        </div>
      </div>
    </div>
  </div>
</div>