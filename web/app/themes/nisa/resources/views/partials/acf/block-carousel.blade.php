@php
  $img = get_sub_field('image');
  $img_position = get_sub_field('image_position'); 
  
  $size = wp_is_mobile() ? 'medium' : 'large';
  $order = $img_position == 'right' ? 'reorder' : '';
  $content_position = $img_position == 'right' ? 'left' : 'right';
  var_dump($img)
@endphp

<div class='container'>
<div class='row'>
  <div class='carousel owl-carousel owl-theme col-md-12'> 
  @while ( have_rows('carousel') )
  <?php ($img_position); ?>
    @php(the_row())
    <div class='carousel__item {{ $order }}'>
      <div class='col-md-9 carousel__image' style="background-image: url('<?= $img['sizes'][$size]; ?>')"></div>
      <div class='col-md-4 carousel__content {{ $content_position }}'>
        <div class='carousel__content-text'>
          {!! get_sub_field('wysiwyg') !!}
        </div>
      </div>
    </div>
  @endwhile
  </div>
</div>
</div>
