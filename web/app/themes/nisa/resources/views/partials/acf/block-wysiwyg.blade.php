<?php
  $text = get_sub_field('text');
  $alignment = get_sub_field('alignment');
  $percentage = get_sub_field('width');
  $position = $alignment == 'left' ? 'left' : 'right';
  $width = $percentage == '75' ? 'wysiwyg__75' : 'wysiwyg__100';
?>

<!-- {{$width}} -->

<div class='container wysiwyg {{$position}}'>
    <div class='col-md-9'>
        {!! $text !!}
    </div>
</div>
