{{-- @php var_dump(get_field('content_blocks')); @endphp --}}
@if ( have_rows('content_blocks') )
  @while ( have_rows('content_blocks') )
  @php(the_row())
    @includeIf('partials.acf.block-'.get_row_layout())
  @endwhile
  
@endif