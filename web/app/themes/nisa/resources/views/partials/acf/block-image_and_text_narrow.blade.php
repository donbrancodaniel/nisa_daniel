@php
  $text = get_sub_field('text');
  $aligned = get_sub_field('alignment');  

  $size = wp_is_mobile() ? 'medium' : 'large';
  $img_position = get_sub_field('image_position');
  $img = get_sub_field('image');

  $img_col_class = $img_position == 'left' ? 'image_and_text_narrow reorder' : 'image_and_text_narrow';
  $content_position = $img_position == 'left' ? 'left' : 'right';
  // var_dump($img);
@endphp

<div class='container block2'>
  <div class='row'>
    <div class='col-md-12 {{ $img_col_class }} {{ $aligned }}'>
      <div class='col-md-6 image_and_text_narrow__content'>
        <div class='image_and_text_narrow__content-text'>
          {!! $text !!}
        </div>
      </div>
        <div class='col-md-4 image_and_text_narrow__image {{ $content_position}}' style="background-image: url('<?= $img['sizes'][$size]; ?>')"></div>
    </div>
  </div>
</div>


