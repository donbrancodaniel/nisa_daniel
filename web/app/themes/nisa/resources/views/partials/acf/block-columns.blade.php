<div class='container '>
    <div class='columns'>

  @while ( have_rows('columns') )

  @php(the_row())
    <div class='col-md-4'>
      <div class='columns__image'>
        {!! wp_get_attachment_image(get_sub_field('image')['ID'], 'nisa-sq-750') !!}
        <div class='columns__text'>
          {!! get_sub_field('text') !!}
        </div>
      </div>
    </div>

  @endwhile
  
</div>


</div>
