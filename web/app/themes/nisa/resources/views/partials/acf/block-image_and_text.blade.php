<?php
  $text = get_sub_field('wysiwyg');
  $img = get_sub_field('image');

  $size = wp_is_mobile() ? 'medium' : 'large';
  $position = get_sub_field('image_position');
  $img_position = $position == 'left' ? 'reorder' : '';
  $content_position = $position == 'left' ? 'left' : 'right';
?>

<div class='container-fluid'>
  <div class='row'>
    <div class='col-md-12 image_and_text {{ $img_position }}'>
      <div class='col-md-9 image_and_text__image' style="background-image: url('<?= $img['sizes'][$size]; ?>')"></div>
      <div class='col-md-5 image_and_text__content {{ $content_position }}'>
        {!! $text !!}
      </div>
    </div>
  </div>
</div>


