<footer class="content-info">
  <div class="container">
    <h2 class="footer-title">Nisa-tuote Oy</h2>
    @php
    $columns = get_field('footer_columns', 'option');
    @endphp
    <div class="row">
      <div class="col-md-3">
        {!! $columns['footer_column_1'] !!}
      </div>
      <div class="col-md-3">
        {!! $columns['footer_column_2'] !!}
      </div>
      <div class="col-md-3">
        {!! $columns['footer_column_3'] !!}
      </div>
      <div class="col-md-3">
        <h5>{{ __('Seuraa meitä', 'nisa') }}</h5>
        @if(get_field('facebook_address', 'option'))
          <a href="{{ get_field('facebook_address', 'option') }}" target="_blank" title="Facebook"><span class="icon-facebook"></span></a>
        @endif
        @if(get_field('twitter_address', 'option'))
          <a href="{{ get_field('twitter_address', 'option') }}" target="_blank" title="Twitter"><span class="icon-twitter"></span></a>
        @endif
        @if(get_field('instagram_address', 'option'))
          <a href="{{ get_field('instagram_address', 'option') }}" target="_blank" title="Instagram"><span class="icon-instagram"></span></a>
        @endif
      </div>
    </div>
  </div>
</footer>
