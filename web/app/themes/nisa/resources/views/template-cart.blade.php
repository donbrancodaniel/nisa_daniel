{{--
  Template Name: Tarjouspyyntökori
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

  <div class="container-fluid container-gray">
    <div class="container">

      <div class="row">
        <div class="col-lg-6">
          @isset($_GET['kiitos'])
            <h1>Kiitos!</h1>
            <p class="lead">{{ __('Olemme sinuun yhteydessä mahdollisimman pian.', 'nisa') }}</p>
            <a href="/" class="button">{{ __('Takaisin etusivulle', 'nisa') }}</a>
          @else
            <h1>{!! App::title() !!}</h1>
            <p class="introduction">{{ __('Hinnat ovat ohjehintoja, eivätkä sisällä painatusta. Tarjouspyynnön lähetettyäsi saat tarkan hinnan tuotteelle.', 'nisa') }}</p>
          @endisset
        </div>
        <div class="col-lg-6">
          {!! App::nisa_steps() !!}
        </div>
      </div>

      @if(!$cart_is_empty)
      <div class="row">
        <div class="col-12">
          <h3>{{ __('Kyllä kiitos, haluan tarjouspyynnön seuraavista tuotteista:', 'nisa') }}</h3>
        </div>
      </div>
      @endif

      @if($cart_is_empty)
        <h4>{{ __('Tarjouspyyntökori on tyhjä.', 'nisa') }}</h4>
        <p>{!! __('Lisää tuotteita koriin <a href="/tuotteet">valikoimastamme »</a>') !!}</p>
      @else
        @include('partials.cart-contents')
      @endif

      @if(!$cart_is_empty)
      <div class="row">
        <div class="col-12">
          <h3>{{ __('Täydennä vielä yhteystietosi ja lisää painotiedosto', 'nisa') }}</h3>
        </div>
      </div>

      <form id="cart-form" method="post">
        <input type="hidden" name="action" value="send-offer-cart">
        <div class="row">
          <div class="col-md-4">
            <input name="customer_name" type="text" placeholder="{{ __('Tilaaja', 'nisa') }}" autocomplete="name">
            <input name="customer_company" type="text" placeholder="{{ __('Yritys', 'nisa') }}">
            <input name="customer_email" type="email" placeholder="{{ __('Sähköpostiosoite', 'nisa') }}" autocomplete="email">
          </div>
          <div class="col-md-4">
            <input name="customer_phone" type="text" placeholder="{{ __('Puhelinnumero', 'nisa') }}" autocomplete="tel">
            <input name="customer_address" type="text" placeholder="{{ __('Katuosoite', 'nisa') }}" autocomplete="shipping street-address">
            <div class="row">
              <div class="col-md-6">
                <input name="customer_city" type="text" placeholder="{{ __('Postitoimipaikka', 'nisa') }}"  autocomplete="shipping address-level2">
              </div>
              <div class="col-md-6">
                <input name="customer_postal_code" type="text" placeholder="{{ __('Postinumero', 'nisa') }}"  autocomplete="shipping postal-code">
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <textarea name="customer_message" placeholder="{{ __('Viesti', 'nisa') }}"></textarea>
          </div>
          <div class="col-md-4">
            <p>{{ __('Jos mahdollista, liitä mukaan painettava aineisto tarjouspyynnön laskemiseksi. Tarvittava painovärien määrä arvioidaan lähettämäsi kuvan mukaan.', 'nisa') }} <strong><a href="#">{{ __('» Katso aineisto-ohjeet', 'nisa') }}</a></strong></p>
            <div id="pond">
              <input name="customer_file[]" type="file" multiple>
            </div>
          </div>
          <div class="col-md-4">
            <p><strong>{{ __('Haluan että minuun ollaan yhteydessä', 'nisa') }}</strong></p>
            <label><input name="customer_contact_method" type="radio" value="email" checked>{{ __('Sähköpostilla', 'nisa') }}</label>
            <label><input name="customer_contact_method" type="radio" value="phone">{{ __('Puhelimitse', 'nisa') }}</label>
          </div>
          <div class="col-md-4 col-submit">
            <input class="button" type="submit" value="{{ __('Lähetä tarjouspyyntö', 'nisa') }}">
          </div>
        </div>
      </form>
      @endif

    </div>
  </div>

  @endwhile
@endsection
